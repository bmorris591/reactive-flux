import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.Detekt
import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

plugins {
    `java-library`
    `project-report`
    jacoco
    `maven-publish`
    alias(libs.plugins.release)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.dokka)
    alias(libs.plugins.git.props)
    alias(libs.plugins.build.info)
    alias(libs.plugins.versions)
    alias(libs.plugins.catalog.update)

    alias(libs.plugins.download)

    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)

    alias(libs.plugins.jmh)
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

group = "uk.co.borismorris.reactiveflux"

repositories {
    mavenCentral()
    maven("https://repo.spring.io/milestone")
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "17"
        allWarningsAsErrors = true
        freeCompilerArgs = listOfNotNull(
            "-Xjsr305=strict",
            "-Werror",
            "-progressive",
            "-opt-in=kotlin.RequiresOptIn",
            "-opt-in=kotlin.ExperimentalUnsignedTypes",
            "-opt-in=kotlin.ExperimentalStdlibApi",
            "-opt-in=kotlinx.coroutines.FlowPreview",
            "-opt-in=kotlinx.coroutines.ExperimentalCoroutinesApi",
        )
    }
}

testing {
    suites {
        val test by getting(JvmTestSuite::class)
        val integrationTest by registering(JvmTestSuite::class) {
            testType.set(TestSuiteType.INTEGRATION_TEST)

            dependencies {
                implementation(project())
            }

            targets {
                all {
                    testTask.configure {
                        shouldRunAfter(test)
                    }
                }
            }
        }

        withType<JvmTestSuite> {
            useJUnitJupiter()
            targets {
                all {
                    testTask.configure {
                        testLogging {
                            exceptionFormat = TestExceptionFormat.FULL
                            showStandardStreams = true
                            events("skipped", "failed")
                        }
                    }
                }
            }
        }
    }
}

val integrationTestImplementation by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}
val integrationTestRuntimeOnly by configurations.getting {
    extendsFrom(configurations.testRuntimeOnly.get())
}

configurations.all {
    exclude(module = "spring-boot-starter-logging")
    exclude(module = "javax.annotation-api")
    exclude(module = "hibernate-validator")
}

dependencies {
    implementation(platform(libs.spring.boot))
    implementation(platform(libs.kotlinx.coroutines))

    api("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8")

    api(libs.flux.dsl) {
        exclude("com.squareup.okhttp3", "okhttp")
        exclude("com.google.code.findbugs5", "jsr305")
    }

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("org.springframework:spring-web")
    implementation("org.springframework:spring-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(libs.opencsv)

    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    implementation(libs.kotlin.logging)

    testImplementation("org.apache.httpcomponents.client5:httpclient5")
    testImplementation("org.apache.httpcomponents.core5:httpcore5-reactive")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation(libs.wiremock)
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test")
    testImplementation(kotlin("test-junit5"))

    testRuntimeOnly(platform(libs.log4j))
    testRuntimeOnly("org.apache.logging.log4j:log4j-core")
    testRuntimeOnly("org.apache.logging.log4j:log4j-slf4j2-impl")

    integrationTestImplementation(platform(libs.testcontainers))
    integrationTestImplementation("org.testcontainers:junit-jupiter")
    integrationTestImplementation("org.testcontainers:influxdb")

    jmh(libs.influx.java)
}

spotless {
    kotlin {
        ktlint()
            .setUseExperimental(true)
    }
    kotlinGradle {
        ktlint()
            .setUseExperimental(true)
    }
}

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    dest("$buildDir/detket.config")
    onlyIfModified(true)
    useETag("all")
}

detekt {
    buildUponDefaultConfig = true
    allRules = true
    config = files(downloadDetektConfig.get().dest)
}

tasks["check"].dependsOn("detektMain")

tasks.withType<Detekt> {
    jvmTarget = "17"
    dependsOn(downloadDetektConfig)
}

jacoco {
    toolVersion = "0.8.8"
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(false)
        csv.required.set(false)
        html.required.set(true)
    }
}

tasks["check"].finalizedBy(tasks.jacocoTestReport)

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = "0.8".toBigDecimal()
            }
        }
    }
}

release {
    preCommitText.set("[release]")
    git {
        requireBranch.set("master")
    }
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("javadoc"))
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
}

val sourcesJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Source code packaged as Jar"
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        create<MavenPublication>("reactive-flux") {
            from(components["java"])
            artifact(sourcesJar)
            artifact(dokkaJar)

            pom {
                name.set(project.name)
                description.set("An InfluxDB client implemented using Spring WebClient")
                url.set("https://gitlab.com/bmorris591/reactive-flux/wikis/home")

                scm {
                    url.set("https://gitlab.com/bmorris591/reactive-flux")
                    connection.set("git@gitlab.com:bmorris591/reactive-flux.git")
                    developerConnection.set("git@gitlab.com:bmorris591/reactive-flux.git")
                }

                licenses {
                    license {
                        name.set("GNU GPLv3")
                        url.set("https://gitlab.com/bmorris591/reactive-flux/blob/master/LICENSE")
                        distribution.set("repo")
                    }
                }

                developers {
                    developer {
                        id.set("bmorris591")
                        name.set("Boris Morris")
                        email.set("bmorris591@gmail.com")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            name = "gitlab-ci"
            url = uri("https://gitlab.com/api/v4/projects/10133668/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("token-header")
            }
        }
    }
}

jmh {
    iterations.set(5)
    benchmarkMode.set(listOf("thrpt"))
    fork.set(1)
    failOnError.set(true)
    forceGC.set(true)
    warmupForks.set(1)
    warmupIterations.set(2)
    profilers.set(listOf("gc"))
}
