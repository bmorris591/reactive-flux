package uk.co.borismorris.reactiveflux

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.api.fail
import org.springframework.http.client.reactive.HttpComponentsClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import uk.co.borismorris.reactiveflux.webclient.WebclientInfluxClient
import java.lang.Math.PI
import java.net.URL
import java.time.Duration
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit
import java.util.function.Consumer

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class InfluxMeasurementTest {

    @JvmField
    @RegisterExtension
    val wiremock = WireMockExtension.newInstance()
        .options(
            options()
                .dynamicPort()
                .usingFilesUnderClasspath("uk/co/borismorris/reactiveflux/wiremock")
                .notifier(ConsoleNotifier(false)),
        )
        .configureStaticDsl(true)
        .build()

    lateinit var props: InfluxDbProps
    lateinit var webclient: WebClient.Builder

    @BeforeEach
    fun setupClient() {
        props = InfluxDbProps().apply {
            url = URL(wiremock.runtimeInfo.httpBaseUrl)
            database = "testdb"
        }
        webclient = WebClient.builder().clientConnector(HttpComponentsClientHttpConnector())
    }

    @Test
    fun `When a single measurement is saved, it is serialised and transmitted`() = runBlocking<Unit> {
        val influxclient = noConverterClient()
        val measurement = createMeasurement("test-measurement")

        val result = influxclient.write(flowOf(measurement)).toList()

        assertThat(result).singleElement().satisfies(
            Consumer {
                assertThat(it.requestId).isEqualTo("test-request-id")
            },
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a single string is saved, it is converted and transmitted`() = runBlocking<Unit> {
        val influxclient = stringToMeasurementConverterClient()

        val result = influxclient.convertAndWrite(flowOf("test-measurement")).toList()

        assertThat(result).singleElement().satisfies(
            Consumer {
                assertThat(it.requestId).isEqualTo("test-request-id")
            },
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When an error is encountered then it is returned to the user`() = runBlocking<Unit> {
        val influxclient = noConverterClient()
        val measurement = createMeasurement("error-measurement")

        try {
            influxclient.write(flowOf(measurement)).toList()
            fail { "Should throw an error" }
        } catch (e: InfluxException) {
            assertThat(e).hasMessageContaining("unable to parse 'foo bar': bad timestamp")
            assertThat(e.error).satisfies(
                Consumer {
                    assertThat(it.error).isEqualTo("unable to parse 'foo bar': bad timestamp")
                    assertThat(it.body).isEqualTo("{\"error\":\"unable to parse 'foo bar': bad timestamp\"}")
                    assertThat(it.requestId).isEqualTo("test-request-id")
                },
            )
        }
        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When all the optional properties are set, they are sent as query params`() = runBlocking<Unit> {
        props.apply {
            organization = "an-organization"
            consistency = Consistency.ANY
            precision = Precision.MINUTES
            retentionPolicy = "retention-policy"
            authToken = "jim:supers3cr3t"
        }

        val influxclient = noConverterClient()
        val measurement = createMeasurement("options-measurement")

        val result = influxclient.write(flowOf(measurement)).toList()

        assertThat(result).singleElement().satisfies(
            Consumer {
                assertThat(it.requestId).isEqualTo("test-request-id")
            },
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a multiple measurements are saved, they are batched and transmitted`() = runBlocking<Unit> {
        props.batchSize = 3
        props.batchDuration = Duration.ofSeconds(5)

        val influxclient = noConverterClient()
        val measurement1 = createMeasurement("first-measurement")
        val measurement2 = createMeasurement("second-measurement")
        val measurement3 = createMeasurement("third-measurement")

        val result = influxclient.write(flowOf(measurement1, measurement2, measurement3)).toList()

        assertThat(result).singleElement().satisfies(
            Consumer {
                assertThat(it.requestId).isEqualTo("test-request-id")
            },
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a batch with a single measurement is saved, it is serialised and transmitted`() = runBlocking<Unit> {
        val influxclient = noConverterClient()
        val batch = measurementBatch {
            measurement(createMeasurement("test-measurement"))
        }

        val result = influxclient.writeBatch(flowOf(batch)).toList()

        assertThat(result).singleElement().satisfies(
            Consumer {
                assertThat(it.requestId).isEqualTo("test-request-id")
            },
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a batch with multiple measurements are saved, they are batched and transmitted`() = runBlocking<Unit> {
        val influxclient = noConverterClient()
        val batch = measurementBatch {
            measurement(createMeasurement("first-measurement"))
            measurement(createMeasurement("second-measurement"))
            measurement(createMeasurement("third-measurement"))
        }

        val result = influxclient.writeBatch(flowOf(batch)).toList()

        assertThat(result).singleElement().satisfies(
            Consumer {
                assertThat(it.requestId).isEqualTo("test-request-id")
            },
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a flux query is sent then response is parsed`() = runBlocking<Unit> {
        props.apply {
            organization = "an-organization"
        }
        val influxclient = noConverterClient()

        val result = influxclient.read { range(-1, 1, ChronoUnit.DAYS) }

        assertThat(result.requestId).isEqualTo("test-request-id")
        val data = result.result.toList()
        assertThat(data).hasSize(60)
        val tables = data.groupBy { it.tableId }
        assertThat(tables)
            .hasSize(3)
            .allSatisfy { tableId, table ->
                assertThat(tableId).isNotNull()
                assertThat(table).hasSize(20).allSatisfy(
                    Consumer {
                        assertThat(it.tableId).isEqualTo(tableId)
                    },
                )
            }

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/query")))
    }

    private fun noConverterClient() = WebclientInfluxClient(props, webclient, emptyList())

    private fun stringToMeasurementConverterClient() = WebclientInfluxClient(props, webclient, listOfNotNull(StringToMeasurementConverter()))
}

internal class StringToMeasurementConverter : InfluxMeasurementConverter {
    override fun convert(source: Any) = flow {
        emit(createMeasurement(source as String))
    }

    override fun canConvert(source: Any) = source is String
}

fun createMeasurement(name: String, time: Int = 1000, precision: TimeUnit = TimeUnit.MINUTES) = measurement(name) {
    time(time.toLong())
    precision(precision)
    tag("test-tag") to "test-tag-value"
    field("test-field") to PI
}
