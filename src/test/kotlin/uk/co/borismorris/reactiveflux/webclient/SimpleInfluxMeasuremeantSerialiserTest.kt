package uk.co.borismorris.reactiveflux.webclient

import kotlinx.coroutines.reactive.asPublisher
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import reactor.test.StepVerifier
import uk.co.borismorris.reactiveflux.Field
import uk.co.borismorris.reactiveflux.Field.Companion.field
import uk.co.borismorris.reactiveflux.InfluxMeasurement
import uk.co.borismorris.reactiveflux.Tag
import uk.co.borismorris.reactiveflux.Tag.Companion.tag
import uk.co.borismorris.reactiveflux.measurement
import java.io.ByteArrayOutputStream
import java.math.BigDecimal
import java.math.RoundingMode.HALF_EVEN
import java.nio.charset.StandardCharsets.UTF_8
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.HOURS
import java.util.concurrent.TimeUnit.MINUTES
import java.util.concurrent.TimeUnit.NANOSECONDS
import java.util.concurrent.TimeUnit.SECONDS

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class SimpleInfluxMeasuremeantSerialiserTest {
    private val serializer = SimpleInfluxMeasuremeantSerialiser()
    private val bufferFactory = DefaultDataBufferFactory()

    @ParameterizedTest(name = "When I serialise a {0}, I receive a valid InfluxDB line protocol")
    @MethodSource("createMeasurements")
    fun `When I serialise a measurement, I receive a valid InfluxDB line protocol`(measurement: InfluxMeasurement, precision: TimeUnit, lineProtocol: String) = runTest {
        val data = serializer.serialise(measurement, precision, bufferFactory).asPublisher()

        val baos = ByteArrayOutputStream()
        StepVerifier.create(DataBufferUtils.write(data, baos))
            .expectNextCount(1)
            .verifyComplete()
        assertThat(String(baos.toByteArray(), UTF_8)).isEqualTo(lineProtocol)
    }

    @Test
    fun `When I serialize a large measurement, multiple buffers are populated`() = runTest {
        val measurement = measurement("large measurement") {
            time(123456789)
            (1..1000).forEach {
                tag("tag-$it") to "value-$it"
                field("field-$it") to it
            }
        }

        val data = serializer.serialise(measurement, NANOSECONDS, bufferFactory).asPublisher()

        val baos = ByteArrayOutputStream()
        StepVerifier.create(DataBufferUtils.write(data, baos))
            .expectNextCount(32)
            .verifyComplete()
        assertThat(String(baos.toByteArray(), UTF_8))
            .hasSize(32_607)
            .startsWith("large\\ measurement,")
            .endsWith("123456789000000\n")
            // example of a string append that overflows the buffer
            .contains("field-270=270i")
    }

    fun createMeasurements() = listOfNotNull(
        arguments(createMeasurement("Simplemeasurement"), MINUTES, "Simplemeasurement 1000\n"),
        // measurement name
        arguments(createMeasurement("Escape spaces in name"), MINUTES, "Escape\\ spaces\\ in\\ name 1000\n"),
        arguments(createMeasurement("Escape,inname"), MINUTES, "Escape\\,inname 1000\n"),
        arguments(createMeasurement("Escape=inname"), MINUTES, "Escape\\=inname 1000\n"),
        arguments(createMeasurement("When the name is complex, then escapes=good"), MINUTES, "When\\ the\\ name\\ is\\ complex\\,\\ then\\ escapes\\=good 1000\n"),
        // line protocol structure
        arguments(createMeasurement("First_tags_then_fields", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to 123)), MINUTES, "First_tags_then_fields,tag=100 field=123i 1000\n"),
        arguments(createMeasurement("Multiple_tags", 1000, MINUTES, mapOf(tag("tag1") to "100", tag("tag2") to "200"), mapOf(field("field") to 123)), MINUTES, "Multiple_tags,tag1=100,tag2=200 field=123i 1000\n"),
        arguments(createMeasurement("Multiple_fields", 1000, MINUTES, mapOf(tag("tag") to 100), mapOf(field("field1") to 123, field("field2") to 321)), MINUTES, "Multiple_fields,tag=100 field2=321i,field1=123i 1000\n"),
        // escaping of tag names
        arguments(createMeasurement("Escape_space_in_tag_name", 1000, MINUTES, mapOf(tag("tag one") to "100"), mapOf(field("field") to 123)), MINUTES, "Escape_space_in_tag_name,tag\\ one=100 field=123i 1000\n"),
        arguments(createMeasurement("Escape_comma_in_tag_name", 1000, MINUTES, mapOf(tag("tag,one") to 100), mapOf(field("field") to 123)), MINUTES, "Escape_comma_in_tag_name,tag\\,one=100 field=123i 1000\n"),
        arguments(createMeasurement("Escape_equals_in_tag_name", 1000, MINUTES, mapOf(tag("tag=one") to 100), mapOf(field("field") to 123)), MINUTES, "Escape_equals_in_tag_name,tag\\=one=100 field=123i 1000\n"),
        // null tags and fields
        arguments(createMeasurement("Null_tag_is_skipped", 1000, MINUTES, mapOf(tag("tag one") to null, tag("tag two") to 100), mapOf(field("field") to 123)), MINUTES, "Null_tag_is_skipped,tag\\ two=100 field=123i 1000\n"),
        arguments(createMeasurement("Null_field_is_skipped", 1000, MINUTES, mapOf(tag("tag,one") to 100), mapOf(field("field") to null, field("fieldtwo") to 123)), MINUTES, "Null_field_is_skipped,tag\\,one=100 fieldtwo=123i 1000\n"),
        // escaping of tag values
        arguments(createMeasurement("Escape_space_in_tag_value", 1000, MINUTES, mapOf(tag("tag") to "100 200"), mapOf(field("field") to 123)), MINUTES, "Escape_space_in_tag_value,tag=100\\ 200 field=123i 1000\n"),
        arguments(createMeasurement("Escape_comma_in_tag_value", 1000, MINUTES, mapOf(tag("tag") to "100,00"), mapOf(field("field") to 123)), MINUTES, "Escape_comma_in_tag_value,tag=100\\,00 field=123i 1000\n"),
        arguments(createMeasurement("Escape_equals_in_tag_value", 1000, MINUTES, mapOf(tag("tag") to "100=200"), mapOf(field("field") to 123)), MINUTES, "Escape_equals_in_tag_value,tag=100\\=200 field=123i 1000\n"),
        // escape all tag parts
        arguments(createMeasurement("Escape_tag_key_and_value", 1000, MINUTES, mapOf(tag("complex tag, idea=bad") to "100,00 = 200,00"), mapOf(field("field") to 123)), MINUTES, "Escape_tag_key_and_value,complex\\ tag\\,\\ idea\\=bad=100\\,00\\ \\=\\ 200\\,00 field=123i 1000\n"),
        // escaping of field names
        arguments(createMeasurement("Escape_space_in_field_name", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field one") to 123)), MINUTES, "Escape_space_in_field_name,tag=100 field\\ one=123i 1000\n"),
        arguments(createMeasurement("Escape_comma_in_field_name", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field,one") to 123)), MINUTES, "Escape_comma_in_field_name,tag=100 field\\,one=123i 1000\n"),
        arguments(createMeasurement("Escape_equals_in_field_name", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field=one") to 123)), MINUTES, "Escape_equals_in_field_name,tag=100 field\\=one=123i 1000\n"),
        // escaping of field values
        arguments(createMeasurement("Escape_quotes_in_field_value", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to "123\"321")), MINUTES, "Escape_quotes_in_field_value,tag=100 field=\"123\\\"321\" 1000\n"),
        arguments(createMeasurement("Escape_backslash_in_field_value", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to "123\\45")), MINUTES, "Escape_backslash_in_field_value,tag=100 field=\"123\\\\45\" 1000\n"),
        // numeric field values reported without quotes
        arguments(createMeasurement("Double_field_value", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to 123.45)), MINUTES, "Double_field_value,tag=100 field=123.45 1000\n"),
        arguments(createMeasurement("Float_field_value", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to 12.375F)), MINUTES, "Float_field_value,tag=100 field=12.375 1000\n"),
        arguments(createMeasurement("BigDecimal_field_value", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to BigDecimal("123.45"))), MINUTES, "BigDecimal_field_value,tag=100 field=123.45 1000\n"),
        // minimum precision is 1
        arguments(createMeasurement("Double_field_value_1dp", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to 123.toDouble())), MINUTES, "Double_field_value_1dp,tag=100 field=123.0 1000\n"),
        arguments(createMeasurement("Float_field_value_1dp", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to 12F)), MINUTES, "Float_field_value_1dp,tag=100 field=12.0 1000\n"),
        arguments(createMeasurement("BigDecimal_field_value_1dp", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to BigDecimal("123"))), MINUTES, "BigDecimal_field_value_1dp,tag=100 field=123.0 1000\n"),
        // maximum precision of 340DP (only applicable to BigDecimal)
        arguments(createMeasurement("BigDecimal_field_value_340dp", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to PI)), MINUTES, "BigDecimal_field_value_340dp,tag=100 field=${PI.setScale(340, HALF_EVEN)} 1000\n"),
        // time is adjusted to precision
        arguments(createMeasurement("No_time_adjust", 1000, MINUTES), MINUTES, "No_time_adjust 1000\n"),
        arguments(createMeasurement("Time_more_precise", 1000, SECONDS), MINUTES, "Time_more_precise 16\n"),
        arguments(createMeasurement("Time_less_precise", 1000, HOURS), MINUTES, "Time_less_precise 60000\n"),
        // object tostring is called
        arguments(createMeasurement("Field_value_tostring", 1000, MINUTES, mapOf(tag("tag") to "100"), mapOf(field("field") to StringBuilder("Some random content"))), MINUTES, "Field_value_tostring,tag=100 field=\"Some random content\" 1000\n"),
    )

    private fun createMeasurement(name: String, time: Int = 1000, precision: TimeUnit = MINUTES, tags: Map<Tag, Any?> = emptyMap(), fields: Map<Field, Any?> = emptyMap()) = measurement(name) {
        time(time.toLong())
        precision(precision)
        tags(tags)
        fields(fields)
    }
}

// PI to 400dp
private val PI = BigDecimal(
    "3." +
        "1415926535" +
        "8979323846" +
        "2643383279" +
        "5028841971" +
        "6939937510" +
        "5820974944" +
        "5923078164" +
        "0628620899" +
        "8628034825" +
        "3421170679" +
        "8214808651" +
        "3282306647" +
        "0938446095" +
        "5058223172" +
        "5359408128" +
        "4811174502" +
        "8410270193" +
        "8521105559" +
        "6446229489" +
        "5493038196" +
        "4428810975" +
        "6659334461" +
        "2847564823" +
        "3786783165" +
        "2712019091" +
        "4564856692" +
        "3460348610" +
        "4543266482" +
        "1339360726" +
        "0249141273" +
        "7245870066" +
        "0631558817" +
        "4881520920" +
        "9628292540" +
        "9171536436" +
        "7892590360" +
        "0113305305" +
        "4882046652" +
        "1384146951" +
        "9415116094",
)
