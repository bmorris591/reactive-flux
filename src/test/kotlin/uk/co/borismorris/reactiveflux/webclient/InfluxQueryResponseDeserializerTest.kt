package uk.co.borismorris.reactiveflux.webclient

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.core.ResolvableType
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.ReactiveHttpInputMessage
import reactor.test.StepVerifier
import uk.co.borismorris.reactiveflux.InfluxResponseRow

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class InfluxQueryResponseDeserializerTest {

    val dataBufferFactory = DefaultDataBufferFactory()
    val deserializer = InfluxQueryResponseDeserializer()

    @Test
    fun `A multi-table response is deserialized into tables`() {
        val msg = object : ReactiveHttpInputMessage {
            override fun getHeaders() = HttpHeaders.EMPTY

            override fun getBody() = DataBufferUtils.read(ClassPathResource("/uk/co/borismorris/reactiveflux/webclient/influxdb_multitable_response.csv"), dataBufferFactory, 1024)
        }
        val type = ResolvableType.forClass(InfluxResponseRow::class.java)

        StepVerifier.withVirtualTime { deserializer.read(type, msg, mutableMapOf()) }
            .expectNextCount(9)
            .verifyComplete()
    }
}
