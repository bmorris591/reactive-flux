package uk.co.borismorris.reactiveflux.webclient

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import org.openjdk.jmh.annotations.Benchmark
import org.openjdk.jmh.annotations.Scope
import org.openjdk.jmh.annotations.State
import org.openjdk.jmh.infra.Blackhole
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import uk.co.borismorris.reactiveflux.measurement
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

open class InfluxMeasuremeantSerialiserTest {

    @OptIn(DelicateCoroutinesApi::class)
    @State(Scope.Benchmark)
    open class ReactiveState {
        val ctx = newFixedThreadPoolContext(Runtime.getRuntime().availableProcessors(), "benchmark")
        val ser = SimpleInfluxMeasuremeantSerialiser()
        var m = measurement("test") {
            time(1662285394076)
            tag("a") to "a"
            tag("b") to 2
            tag("c") to 3.0
            tag("d") to BigDecimal("4.5")
            tag("e") to true

            field("a") to "a"
            field("b") to 2
            field("c") to 3.0
            field("d") to BigDecimal("4.5")
            field("e") to true
        }
        var bufferFactory = DefaultDataBufferFactory()
    }

    @Benchmark
    fun serialiseMeasurementsReactive(blackHole: Blackhole, reactiveState: ReactiveState) = runBlocking(reactiveState.ctx) {
        flow {
            for (i in 1..1000) {
                emit(reactiveState.m)
            }
        }
            .flatMapConcat { reactiveState.ser.serialise(it, TimeUnit.NANOSECONDS, reactiveState.bufferFactory) }
            .collect { blackHole.consume(it) }
    }

    @State(Scope.Benchmark)
    open class SynchronousState {
        var p = Point("test")
            .time(1662285394076, WritePrecision.NS)
            .addTag("a", "a")
            .addTag("b", "2")
            .addTag("c", "3.0")
            .addTag("d", "4.5")
            .addTag("e", "true")
            .addField("a", "a")
            .addField("b", 2)
            .addField("c", 3.0)
            .addField("d", BigDecimal("4.5"))
            .addField("e", true)
    }

    @Benchmark
    fun serialiseMeasurementsSynchronous(blackHole: Blackhole, state: SynchronousState) {
        for (i in 1..1000) {
            blackHole.consume(state.p.toLineProtocol())
        }
    }
}
