package uk.co.borismorris.reactiveflux

import org.springframework.util.CollectionUtils
import org.springframework.util.MultiValueMap
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import java.util.concurrent.TimeUnit

class InfluxQueryParameters(val parameters: Map<String, *>) {
    fun asMultiValueMap(): MultiValueMap<String, String> {
        val mvm = parameters.entries.map { it.key to listOf(it.value.toString()) }.toMap()
        return CollectionUtils.toMultiValueMap(mvm)
    }
}

val DEFAULT_PRECISION = Precision.NANOSECONDS
const val DEFAULT_RETENTION_POLICY = "autogen"
val DEFAULT_CONSISTENCY = Consistency.ONE

class InfluxQueryParameterBuilder {
    lateinit var database: String
    var organization = ""
    var retentionPolicy = DEFAULT_RETENTION_POLICY
    var precision = DEFAULT_PRECISION
    var consistency = DEFAULT_CONSISTENCY

    fun build(): InfluxQueryParameters {
        val parameters = buildMap<String, Any> {
            put(QueryParameter.BUCKET.parameter, database + if (retentionPolicy != DEFAULT_RETENTION_POLICY) "/" + retentionPolicy else "")
            if (organization.isNotBlank()) put(QueryParameter.ORGANIZATION.parameter, organization)
            if (precision != DEFAULT_PRECISION) put(QueryParameter.PRECISION.parameter, precision)
            if (consistency != DEFAULT_CONSISTENCY) put(QueryParameter.CONSISTENCY.parameter, consistency)
        }
        return InfluxQueryParameters(parameters)
    }
}

fun fromInfluxDbProps(influxDbProps: InfluxDbProps) = parameters {
    database = influxDbProps.database
    influxDbProps.organization?.let { organization = it }
    influxDbProps.retentionPolicy?.let { retentionPolicy = it }
    influxDbProps.precision?.let { precision = it }
    influxDbProps.consistency?.let { consistency = it }
}

fun InfluxDbProps.toQueryParameters() = fromInfluxDbProps(this)

inline fun parameters(buildParameters: InfluxQueryParameterBuilder.() -> Unit): InfluxQueryParameters {
    val builder = InfluxQueryParameterBuilder()
    builder.buildParameters()
    return builder.build()
}

enum class Precision(val precision: String, val timeUnit: TimeUnit) {
    NANOSECONDS("ns", TimeUnit.NANOSECONDS),
    MICROSECONDS("us", TimeUnit.MICROSECONDS),
    MILLISECONDS("ms", TimeUnit.MILLISECONDS),
    SECONDS("s", TimeUnit.SECONDS),
    MINUTES("m", TimeUnit.MINUTES),
    HOURS("h", TimeUnit.HOURS), ;

    override fun toString() = precision
}

enum class Consistency(val consistency: String) {
    ANY("any"),
    ONE("one"),
    QUORUM("quorum"),
    ALL("all"), ;

    override fun toString() = consistency
}

enum class QueryParameter(val parameter: String) {
    ORGANIZATION("org"),
    BUCKET("bucket"),
    PRECISION("precision"),
    CONSISTENCY("consistency"),
}
