package uk.co.borismorris.reactiveflux.webclient

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.asPublisher
import mu.two.KLogging
import org.reactivestreams.Publisher
import org.springframework.core.ResolvableType
import org.springframework.core.io.buffer.DataBufferFactory
import org.springframework.http.MediaType
import org.springframework.http.ReactiveHttpOutputMessage
import org.springframework.http.codec.HttpMessageWriter
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import uk.co.borismorris.reactiveflux.DEFAULT_PRECISION
import uk.co.borismorris.reactiveflux.InfluxMeasurement
import uk.co.borismorris.reactiveflux.InfluxMeasurementBatch
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass

sealed class InfluxSerializer<T : Any>(influxDbProps: InfluxDbProps, type: KClass<T>, private val measurementSerialiser: InfluxMeasuremeantSerialiser) : HttpMessageWriter<T> {
    companion object : KLogging()

    private val precision = (influxDbProps.precision ?: DEFAULT_PRECISION).timeUnit
    private val clazz = type.java

    override fun getWritableMediaTypes() = mutableListOf(MediaType.TEXT_PLAIN)

    override fun canWrite(elementType: ResolvableType, mediaType: MediaType?) =
        mediaType?.includes(MediaType.TEXT_PLAIN) ?: false && clazz.isAssignableFrom(elementType.toClass())

    internal fun convertPoints(points: Flow<InfluxMeasurement>, bufferFactory: DataBufferFactory) = points
        .flatMapConcat { it.serialise(measurementSerialiser, precision, bufferFactory) }
        .onEach { logger.trace("Sending buffer {}", it) }
}

@Suppress("ForbiddenVoid")
class InfluxBatchPointSerialiser(
    influxDbProps: InfluxDbProps,
    measurementSerialiser: InfluxMeasuremeantSerialiser,
) : InfluxSerializer<InfluxMeasurementBatch>(influxDbProps, InfluxMeasurementBatch::class, measurementSerialiser) {

    override fun write(
        inputStream: Publisher<out InfluxMeasurementBatch>,
        elementType: ResolvableType,
        mediaType: MediaType?,
        message: ReactiveHttpOutputMessage,
        hints: MutableMap<String, Any>,
    ): Mono<Void> = message.writeAndFlushWith(inputStream.convert(message.bufferFactory())).checkpoint()

    private fun Publisher<out InfluxMeasurementBatch>.convert(bufferFactory: DataBufferFactory) = Flux.from(this)
        .map { convertBatch(it, bufferFactory).asPublisher() }

    private fun convertBatch(batch: InfluxMeasurementBatch, bufferFactory: DataBufferFactory) = convertPoints(batch.asFlow(), bufferFactory)
}

@Suppress("ForbiddenVoid")
class InfluxPointSerialiser(
    influxDbProps: InfluxDbProps,
    measurementSerialiser: InfluxMeasuremeantSerialiser,
) : InfluxSerializer<InfluxMeasurement>(influxDbProps, InfluxMeasurement::class, measurementSerialiser) {

    override fun write(
        inputStream: Publisher<out InfluxMeasurement>,
        elementType: ResolvableType,
        mediaType: MediaType?,
        message: ReactiveHttpOutputMessage,
        hints: MutableMap<String, Any>,
    ): Mono<Void> = message.writeWith(convertPoints(inputStream.asFlow(), message.bufferFactory()).asPublisher()).checkpoint()
}

private fun InfluxMeasurement.serialise(measurementSerialiser: InfluxMeasuremeantSerialiser, precision: TimeUnit?, bufferFactory: DataBufferFactory) = measurementSerialiser.serialise(this, precision, bufferFactory)
