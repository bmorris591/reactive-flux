package uk.co.borismorris.reactiveflux.webclient

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferFactory
import uk.co.borismorris.reactiveflux.InfluxMeasurement
import uk.co.borismorris.reactiveflux.Key
import java.math.BigDecimal
import java.math.BigInteger
import java.nio.CharBuffer
import java.nio.charset.Charset
import java.nio.charset.CoderResult
import java.nio.charset.CodingErrorAction.REPLACE
import java.text.NumberFormat
import java.util.Locale
import java.util.Objects
import java.util.concurrent.TimeUnit
import kotlin.math.min
import kotlin.text.Charsets.UTF_8

interface InfluxMeasuremeantSerialiser {
    fun serialise(measurement: InfluxMeasurement, precision: TimeUnit?, bufferFactory: DataBufferFactory): Flow<DataBuffer>
}

private const val DEFAULT_BUFFER = 1024
private const val MAX_FRACTION_DIGITS = 340

class SimpleInfluxMeasuremeantSerialiser : InfluxMeasuremeantSerialiser {
    private val charSinks = Channel<CharBufferSink>(10).also { channel ->
        runBlocking {
            repeat(10) {
                channel.send(CharBufferSink(UTF_8))
            }
        }
    }

    override fun serialise(measurement: InfluxMeasurement, precision: TimeUnit?, bufferFactory: DataBufferFactory) = encode(bufferFactory) {
        lineProtocol(measurement, precision, it)
    }

    private suspend fun lineProtocol(measurement: InfluxMeasurement, precision: TimeUnit?, sb: CharSink) {
        escapeKey(measurement.measurement, sb)
        if (measurement.tags.isNotEmpty()) sb.append(',')
        concatenatedTags(measurement, sb)
        if (measurement.fields.isNotEmpty()) sb.append(' ')
        concatenatedFields(measurement, sb)
        sb.append(' ')
        sb.append(formattedTime(measurement, precision))
        sb.append('\n')
    }

    private suspend fun concatenatedTags(measurement: InfluxMeasurement, sb: CharSink) {
        measurement.tags.asSequence()
            .concatData(sb, { escapeKey(it.toString(), this) })
    }

    private suspend fun concatenatedFields(measurement: InfluxMeasurement, sb: CharSink) {
        measurement.fields.asSequence()
            .concatData(sb, { escapeFieldValue(it, this) })
    }

    private suspend fun <K : Key, V> Sequence<Map.Entry<K, V>>.concatData(
        sb: CharSink,
        valueProcessor: suspend CharSink.(v: V) -> Unit,
        delimiter: Char = '=',
        entryDelimiter: Char = ',',
    ) {
        concatData(sb, { escapeKey(it.key, this) }, valueProcessor, delimiter, entryDelimiter)
    }

    private suspend fun <K, V> Sequence<Map.Entry<K, V>>.concatData(
        sb: CharSink,
        keyProcessor: suspend CharSink.(k: K) -> Unit,
        valueProcessor: suspend CharSink.(v: V) -> Unit,
        delimiter: Char = '=',
        entryDelimiter: Char = ',',
    ) {
        val iter = this.iterator()
        for (entry in iter) {
            sb.keyProcessor(entry.key)
            sb.append(delimiter)
            sb.valueProcessor(entry.value)
            if (iter.hasNext()) sb.append(entryDelimiter)
        }
    }

    private suspend fun escapeFieldValue(value: Any, sb: CharSink) {
        when (value) {
            is Double, is Float, is BigDecimal -> sb.appendFormatted(value as Number)
            is Byte, is Short, is Int, is Long, is BigInteger -> sb.appendFormatted(value as Number).append('i')
            else -> escapeField(Objects.toString(value), sb)
        }
    }

    private suspend fun escapeKey(key: String, sb: CharSink) {
        escape(sb, key) { it == ' ' || it == ',' || it == '=' }
    }

    private suspend fun escapeField(field: String, sb: CharSink) {
        sb.append('"')
        escape(sb, field) { it == '\\' || it == '"' }
        sb.append('"')
    }

    private suspend fun escape(sb: CharSink, s: String, predicate: (Char) -> Boolean) {
        for (c in s) {
            if (predicate(c)) {
                sb.append('\\')
            }
            sb.append(c)
        }
    }

    private fun formattedTime(measurement: InfluxMeasurement, precision: TimeUnit?) = precision?.convert(measurement.time, measurement.precision) ?: measurement.time

    private fun encode(bufferFactory: DataBufferFactory, encode: suspend (charSink: CharSink) -> Unit) = flow<DataBuffer> {
        val cSink = charSinks.receive()
        try {
            cSink.encode(bufferFactory, this) {
                encode(this)
            }
        } finally {
            charSinks.send(cSink)
        }
    }
}

suspend fun CharBufferSink.encode(bufferFactory: DataBufferFactory, publisher: FlowCollector<DataBuffer>, op: suspend CharSink.() -> Unit) {
    prepare(bufferFactory, publisher)
    try {
        op()
    } finally {
        finish()
    }
}

interface CharSink {
    suspend fun append(c: Char): CharSink
    suspend fun append(c: CharSequence): CharSink
    suspend fun appendFormatted(number: Number): CharSink
    suspend fun append(o: Any): CharSink {
        append(Objects.toString(o))
        return this
    }
}

class CharBufferSink(charSet: Charset) : CharSink {
    private val decimalFormat = NumberFormat.getNumberInstance(Locale.ENGLISH).apply {
        maximumFractionDigits = MAX_FRACTION_DIGITS
        isGroupingUsed = false
        minimumFractionDigits = 1
    }
    private val integerFormat = NumberFormat.getIntegerInstance(Locale.ENGLISH).apply {
        isGroupingUsed = false
    }
    private val charsetEncoder = charSet.newEncoder()
        .onMalformedInput(REPLACE)
        .onUnmappableCharacter(REPLACE)
    private val cBuffer = CharBuffer.allocate(DEFAULT_BUFFER)

    private var bufferFactory: DataBufferFactory? = null
    private var publisher: FlowCollector<DataBuffer>? = null

    fun prepare(bufferFactory: DataBufferFactory, publisher: FlowCollector<DataBuffer>) {
        this.bufferFactory = bufferFactory
        this.publisher = publisher
        charsetEncoder.reset()
        cBuffer.clear()
    }

    suspend fun finish() {
        flushBuffer()
        bufferFactory = null
        publisher = null
    }

    override suspend fun append(c: Char): CharSink {
        flushIfFull()
        cBuffer.append(c)
        return this
    }

    override suspend fun append(c: CharSequence): CharSink {
        var offset = 0
        while (offset < c.length) {
            flushIfFull()
            val length = min(cBuffer.remaining(), c.length - offset)
            cBuffer.append(c, offset, offset + length)
            offset += length
        }
        return this
    }

    override suspend fun appendFormatted(number: Number) = when (number) {
        is Double, is Float, is BigDecimal -> append(decimalFormat.format(number))
        is Byte, is Short, is Int, is Long, is BigInteger -> append(integerFormat.format(number))
        else -> throw IllegalArgumentException("Cannot format $number")
    }

    private suspend fun flushBuffer() {
        if (cBuffer.position() == 0) {
            return
        }
        cBuffer.flip()
        charsetEncoder.reset()
        val outBuffer = (bufferFactory ?: error("Not prepared for write")).allocateBuffer(estimateSize())
        outBuffer.writeChars()
        (publisher ?: error("Not prepared for write")).emit(outBuffer)
        cBuffer.clear()
    }

    private fun DataBuffer.writeChars() {
        var bb = byteBuffer()
        while (true) {
            var cr = if (cBuffer.hasRemaining()) {
                charsetEncoder.encode(cBuffer, bb, true)
            } else {
                CoderResult.UNDERFLOW
            }
            if (cr.isUnderflow) {
                cr = charsetEncoder.flush(bb)
            }
            if (cr.isUnderflow) {
                break
            }
            if (cr.isOverflow) {
                incrementWritePosition(bb.position())
                ensureWritable(maxSize())
                bb = byteBuffer()
            }
        }
        incrementWritePosition(bb.position())
    }

    private fun DataBuffer.incrementWritePosition(increment: Int) {
        writePosition(writePosition() + increment)
    }

    private suspend fun flushIfFull() {
        if (cBuffer.isFull()) flushBuffer()
    }

    private fun CharBuffer.isFull() = !this.hasRemaining()

    private fun maxSize() = (cBuffer.remaining() * charsetEncoder.maxBytesPerChar()).toInt()

    private fun estimateSize() = (cBuffer.remaining() * charsetEncoder.averageBytesPerChar()).toInt()

    @Suppress("DEPRECATION")
    private fun DataBuffer.byteBuffer() = asByteBuffer(writePosition(), writableByteCount())
}
