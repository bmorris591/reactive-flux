package uk.co.borismorris.reactiveflux.webclient

import com.fasterxml.jackson.annotation.JsonValue
import com.influxdb.query.dsl.Flux
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.isActive
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.selects.select
import mu.two.KLogging
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.TEXT_PLAIN
import org.springframework.http.ResponseEntity
import org.springframework.http.codec.ClientCodecConfigurer
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBodilessEntity
import org.springframework.web.reactive.function.client.awaitEntity
import org.springframework.web.reactive.function.client.body
import org.springframework.web.reactive.function.client.toEntityFlux
import reactor.core.publisher.Mono
import uk.co.borismorris.reactiveflux.InfluxClient
import uk.co.borismorris.reactiveflux.InfluxError
import uk.co.borismorris.reactiveflux.InfluxException
import uk.co.borismorris.reactiveflux.InfluxMeasurement
import uk.co.borismorris.reactiveflux.InfluxMeasurementBatch
import uk.co.borismorris.reactiveflux.InfluxMeasurementConverter
import uk.co.borismorris.reactiveflux.InfluxQueryResult
import uk.co.borismorris.reactiveflux.InfluxResponseRow
import uk.co.borismorris.reactiveflux.InfluxWriteSuccess
import uk.co.borismorris.reactiveflux.QueryParameter.ORGANIZATION
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import uk.co.borismorris.reactiveflux.measurementBatch
import uk.co.borismorris.reactiveflux.toQueryParameters
import uk.co.borismorris.reactiveflux.webclient.InfluxQueryResponseDeserializer.Companion.TEXT_CSV
import uk.co.borismorris.reactiveflux.webclient.InfluxResponseHeader.ErrorMessage
import uk.co.borismorris.reactiveflux.webclient.InfluxResponseHeader.RequestId
import java.time.Duration
import java.util.logging.Level

const val API_V2 = "/api/v2"
const val WRITE = "/write"
const val QUERY = "/query"

class WebclientInfluxClient(
    private val influxDbProps: InfluxDbProps,
    private val webClientBuilder: WebClient.Builder,
    private val converters: List<InfluxMeasurementConverter> = emptyList(),
) : InfluxClient {
    companion object : KLogging()

    private val webClient = influxApiWebClient()
    private val queryParams = influxDbProps.toQueryParameters().asMultiValueMap()

    override fun convertAndWrite(points: Flow<*>) = write(points.flatMapConcat { tryConvert(it ?: throw NullPointerException("Null points are not supported")) })

    private fun tryConvert(input: Any) = converters
        .first { it.canConvert(input) }
        .convert(input)

    override fun writeBatch(batch: Flow<InfluxMeasurementBatch>) = postToInflux(batch)

    override fun write(points: Flow<InfluxMeasurement>): Flow<InfluxWriteSuccess> {
        return if (influxDbProps.enableBatching()) {
            writeBatched(points, influxDbProps.batchSize ?: 1, influxDbProps.batchDuration ?: Duration.ofMillis(1))
        } else {
            writeUnbatched(points)
        }
    }

    private fun InfluxDbProps.enableBatching() = batchSize.let { it != null && it > 0 } || batchDuration.let { it != null && it > Duration.ZERO }

    override suspend fun read(query: Flux.() -> Flux) = webClient.post()
        .uri {
            it.path(QUERY).queryParam(ORGANIZATION.parameter, influxDbProps.organization).build()
        }
        .contentType(APPLICATION_JSON)
        .accept(TEXT_CSV)
        .body(Flux.from(influxDbProps.database).query().toPublisher().map { Query(query = it) })
        .receiveHandleError {
            val result = toEntityFlux<InfluxResponseRow>().awaitSingle()
            InfluxQueryResult(result.getDiagnosticHeader(RequestId), result.body!!.asFlow())
        }

    private fun Flux.toPublisher() = Mono.create<String> { it.success(toString()) }
        .log(WebclientInfluxClient::class.qualifiedName, Level.FINEST, true)

    private fun writeBatched(measurements: Flow<InfluxMeasurement>, batchSize: Int, batchTimeout: Duration) = postToInflux(measurements.bufferTimeout(batchSize, batchTimeout).map { it.batch() })

    /**
     * https://dev.to/psfeng/a-story-of-building-a-custom-flow-operator-buffertimeout-4d95
     */
    @OptIn(ObsoleteCoroutinesApi::class)
    @Suppress("SwallowedException")
    private fun <T> Flow<T>.bufferTimeout(size: Int, duration: Duration): Flow<List<T>> {
        require(size > 0) { "Window size should be greater than 0" }
        require(duration.toMillis() > 0) { "Duration should be greater than 0" }

        return flow {
            coroutineScope {
                val events = ArrayList<T>(size)
                val tickerChannel = ticker(duration.toMillis())
                try {
                    val upstreamValues = produce { collect { send(it) } }

                    while (isActive) {
                        var hasTimedOut = false

                        select<Unit> {
                            upstreamValues.onReceive {
                                events.add(it)
                            }

                            tickerChannel.onReceive {
                                hasTimedOut = true
                            }
                        }

                        if (events.size == size || (hasTimedOut && events.isNotEmpty())) {
                            emit(events.toList())
                            events.clear()
                        }
                    }
                } catch (e: ClosedReceiveChannelException) {
                    // drain remaining events
                    if (events.isNotEmpty()) emit(events.toList())
                } finally {
                    tickerChannel.cancel()
                }
            }
        }
    }

    fun List<InfluxMeasurement>.batch() = measurementBatch { this@batch.forEach { measurement(it) } }

    private fun writeUnbatched(measurements: Flow<InfluxMeasurement>) = postToInflux(measurements)

    private suspend inline fun <reified T : Any> post(data: T) = webClient.post()
        .uri { it.path(WRITE).queryParams(queryParams).build() }
        .contentType(TEXT_PLAIN)
        .accept(APPLICATION_JSON)
        .contentType(TEXT_PLAIN)
        .bodyValue(data)
        .receiveHandleError { asWriteSuccess() }

    private suspend fun <T : Any> WebClient.RequestHeadersSpec<*>.receiveHandleError(onSuccess: suspend WebClient.ResponseSpec.() -> T) = retrieve()
        .onStatus({ it.isError }, { mono { it.asError() } })
        .onSuccess()

    private suspend fun ClientResponse.asError() = awaitEntity<String>()
        .let {
            val error = it.getDiagnosticHeader(ErrorMessage)
            val id = it.getDiagnosticHeader(RequestId)
            InfluxException(InfluxError(error, it.body, id))
        }

    private suspend fun WebClient.ResponseSpec.asWriteSuccess() = InfluxWriteSuccess(awaitBodilessEntity().getDiagnosticHeader(RequestId))

    private fun influxApiWebClient() = webClientBuilder
        .baseUrl(influxDbProps.url.toString() + API_V2)
        .defaultHeaders { headers ->
            influxDbProps.authToken?.let { headers.set(AUTHORIZATION, "Token $it") }
        }
        .codecs { configureInfuxApiCodecs(it) }
        .build()

    private fun configureInfuxApiCodecs(it: ClientCodecConfigurer) {
        val serialiser = SimpleInfluxMeasuremeantSerialiser()
        with(it.customCodecs()) {
            register(InfluxBatchPointSerialiser(influxDbProps, serialiser))
            register(InfluxPointSerialiser(influxDbProps, serialiser))
            register(InfluxQueryResponseDeserializer())
        }
    }

    private fun ResponseEntity<*>.getDiagnosticHeader(header: InfluxResponseHeader) = headers[header.header]?.joinToString().orEmpty()

    @JvmName("postMeasurementToInflux")
    private fun postToInflux(measurements: Flow<InfluxMeasurement>) = measurements.map { post(it) }

    @JvmName("postMeasurementBatchToInflux")
    private fun postToInflux(measurements: Flow<InfluxMeasurementBatch>) = measurements.filter { it.isNotEmpty() }.map { post(it) }
}

data class Query(
    val dialect: Dialect = Dialect(),
    val query: String,
    val now: String? = null,
    val type: InfluxResponseType = InfluxResponseType.Flux,
)

enum class InfluxResponseType {
    Flux, ;

    @JsonValue
    fun toType() = name.lowercase()
}

data class Dialect(
    val header: Boolean = true,
    val delimiter: Char = ',',
    val annotations: Set<InfluxResponseAnnotation> = InfluxResponseAnnotation.values().toSet(),
    val commentPrefix: Char = '#',
    val dateTimeFormat: InfluxResponseDateTimeFormat = InfluxResponseDateTimeFormat.Rfc3339Nano,
)

enum class InfluxResponseAnnotation {
    Datatype,
    Group,
    Default, ;

    @JsonValue
    fun toAnnotation() = name.lowercase()
}

enum class InfluxResponseDateTimeFormat(private val format: String) {
    Rfc3339("RFC3339"),
    Rfc3339Nano("RFC3339Nano"), ;

    @JsonValue
    fun toAnnotation() = format
}

enum class InfluxResponseHeader(val header: String) {
    Build("X-Influxdb-Build"),
    ErrorMessage("X-Influxdb-Error"),
    Version("X-Influxdb-Version"),
    RequestId("X-Request-Id"),
}
