@file:Suppress("UnsafeCallOnNullableType")

package uk.co.borismorris.reactiveflux.webclient

import com.opencsv.RFC4180ParserBuilder
import com.opencsv.enums.CSVReaderNullFieldIndicator.BOTH
import mu.two.KLogging
import org.springframework.core.ResolvableType
import org.springframework.core.codec.StringDecoder
import org.springframework.http.MediaType
import org.springframework.http.ReactiveHttpInputMessage
import org.springframework.http.codec.HttpMessageReader
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import uk.co.borismorris.reactiveflux.InfluxResponseRow
import uk.co.borismorris.reactiveflux.InfluxResponseTableColumn
import uk.co.borismorris.reactiveflux.InfluxResponseTableHeader
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.FieldName
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.MeasurementName
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.StartTime
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.StopTime
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.TableId
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.TagStart
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.Time
import uk.co.borismorris.reactiveflux.webclient.ResultColumn.Value
import java.time.Duration
import java.time.Instant
import java.util.Base64
import java.util.concurrent.CopyOnWriteArrayList
import java.util.logging.Level
import kotlin.reflect.KProperty
import kotlin.reflect.full.createInstance

const val TEXT_CSV_VALUE = "text/csv"
const val NUM_HEADER_ROWS = 4

class InfluxQueryResponseDeserializer : HttpMessageReader<InfluxResponseRow> {
    companion object : KLogging() {
        val TEXT_CSV: MediaType = MediaType.parseMediaType(TEXT_CSV_VALUE)
        val MEDIA_TYPES = listOf(TEXT_CSV)

        private val stringType = ResolvableType.forClass(String::class.java)
        private val stringDecoder = StringDecoder.allMimeTypes()
        private val csvReader = RFC4180ParserBuilder()
            .withFieldAsNull(BOTH)
            .build()
    }

    override fun canRead(elementType: ResolvableType, mediaType: MediaType?) = mediaType?.includes(TEXT_CSV) ?: false && InfluxResponseRow::class.java.isAssignableFrom(elementType.toClass())

    override fun readMono(elementType: ResolvableType, message: ReactiveHttpInputMessage, hints: MutableMap<String, Any>): Mono<InfluxResponseRow> = read(elementType, message, hints).next()

    override fun getReadableMediaTypes() = MEDIA_TYPES

    override fun read(elementType: ResolvableType, message: ReactiveHttpInputMessage, hints: MutableMap<String, Any>): Flux<InfluxResponseRow> {
        val contentType = message.headers.contentType ?: TEXT_CSV
        return stringDecoder.decode(message.body, stringType, contentType, hints)
            .windowWhile { it.isNotBlank() }
            .flatMapSequential { parseTables(it) }
            .log(InfluxQueryResponseDeserializer::class.qualifiedName, Level.FINEST, true)
            .checkpoint()
    }

    private fun parseTables(data: Flux<String>): Flux<InfluxResponseRow> = data
        .map { csvReader.parseLine(it) }
        .transformDeferred { rows ->
            Flux.using({ TableParser() }, { parser -> rows.handle(parser::handle) }, {})
        }

    class TableParser {

        private val headerRows = CopyOnWriteArrayList<Array<String?>>()
        private lateinit var header: CsvTableHeader

        fun handle(row: Array<String?>, sink: SynchronousSink<InfluxResponseRow>) {
            when {
                this::header.isInitialized -> {
                    sink.next(CsvResponseRow(header, row))
                }

                row.first()?.startsWith('#') ?: false -> {
                    headerRows.add(row)
                }

                else -> {
                    headerRows.add(row)
                    header = parseHeader(headerRows)
                }
            }
        }

        private fun parseHeader(header: List<Array<String?>>): CsvTableHeader {
            assert(header.size == NUM_HEADER_ROWS) { "Expecting 4 headers" }

            val headersByType = header
                .takeWhile { it.first()?.startsWith('#') ?: false }
                .associateBy { it.first()!!.parseHeader() }
            val tableHeader = header[NUM_HEADER_ROWS - 1]

            assert(headersByType.size == 3) { "Failed to file 3 annotation headers" }
            assert(tableHeader.isNotEmpty()) { "Failed to find the first row after annotations" }
            logger.debug("Processing a table {} {}", headersByType, tableHeader)
            assert(headersByType.values.all { it.size == tableHeader.size })

            val columns = parseColumns(tableHeader, headersByType)
            return CsvTableHeader(columns)
        }

        private fun parseColumns(tableHeader: Array<String?>, headersByType: Map<Header, Array<String?>>) = tableHeader.mapIndexed { i, columnName ->
            val dataType = DataType.parse(headersByType.getValue(Header.DataType)[i]!!)
            val isGroupKey = headersByType.getValue(Header.Group)[i]!!.toBoolean()
            val defaultValue = dataType.parse(headersByType.getValue(Header.Default)[i])
            logger.trace("parsing Column(columnName='{}',dataType='{}',isGroupKey='{}',default='{}')", columnName, dataType, isGroupKey, defaultValue)
            dataType.parseColumn(i, columnName, headersByType.getValue(Header.Group)[i]!!, headersByType.getValue(Header.Default)[i])
        }.toList()

        private fun <T : Any> DataType<T>.parseColumn(index: Int, columnName: String?, isGroupKey: String, defaultValue: String?) =
            CsvTableColumn(index, columnName, this, isGroupKey.toBoolean(), parse(defaultValue)).also {
                logger.trace("{}", it)
            }

        private fun String.parseHeader() = Header.fromHeader(this)
    }
}

enum class Header {
    DataType,
    Group,
    Default, ;

    companion object {
        fun fromHeader(header: String) = if (header.startsWith('#')) {
            findByHeader(header.substring(1))
        } else {
            findByHeader(header)
        }

        private fun findByHeader(header: String) = values().first { it.name.lowercase().equals(header) }
    }
}

data class CsvTableColumn<T : Any>(override val index: Int, override val name: String?, override val dataType: DataType<T>, override val group: Boolean, override val default: T?) : InfluxResponseTableColumn<T>

fun <T : Any> CsvTableColumn<T>.parse(value: String?) = dataType.parse(value) ?: default

fun <T : Any> CsvTableColumn<T>.getValue(row: Array<String?>) = parse(row[index])

data class CsvTableHeader(override val columns: List<CsvTableColumn<out Any>>) : InfluxResponseTableHeader

class CsvResponseRow(override val header: CsvTableHeader, private val rawRow: Array<String?>) : InfluxResponseRow {

    override val tableId: Any by lazyColumn(TableId)
    override val start: Instant by lazyColumn(StartTime)
    override val stop: Instant by lazyColumn(StopTime)
    override val time: Instant by lazyColumn(Time)
    override val value: Any by lazyColumn(Value)
    override val field: String by lazyColumn(FieldName)
    override val measurement: String by lazyColumn(MeasurementName)
    override val tags: Map<String, Any?> by lazy { initTags() }

    inline fun <reified T> lazyColumn(column: ResultColumn) = LazyColumn<T>(column)

    private fun initTags(): Map<String, Any?> = header.columns
        .zip(rawRow)
        .asSequence()
        .drop(TagStart.index())
        .associateBy({ it.first.name!! }, { it.first.parse(it.second) })

    override fun rawValue(index: Int) = rawRow[index]

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CsvResponseRow) return false

        if (header != other.header) return false
        if (!rawRow.contentEquals(other.rawRow)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = header.hashCode()
        result = 31 * result + rawRow.contentHashCode()
        return result
    }

    override fun toString(): String {
        return "InfluxResponseRow(tableId='$tableId',start='$start',stop='$stop',time='$time',value='$value',field='$field',measurement='$measurement',tags='$tags')"
    }

    inner class LazyColumn<T>(private val column: ResultColumn) {

        @Suppress("UNCHECKED_CAST")
        private val lazy = lazy(LazyThreadSafetyMode.PUBLICATION) { getColumn(column) as T }

        private fun getColumn(column: ResultColumn) = header.columns[column.index()].getValue(rawRow)

        operator fun getValue(row: CsvResponseRow, property: KProperty<*>) = lazy.value
    }
}

enum class ResultColumn {
    AnnotationType,
    ResultName,
    TableId,
    StartTime,
    StopTime,
    Time,
    Value,
    FieldName,
    MeasurementName,
    TagStart, ;

    fun index() = ordinal
}

fun Array<String?>.getColumnValue(column: ResultColumn) = get(column.index())

sealed class DataType<T : Any>(val name: String) {
    companion object {
        private val parsers = DataType::class.sealedSubclasses
            .map { it.createInstance() }

        fun parse(input: String) = parsers.first { it.canParse(input) }
    }

    open fun canParse(typeName: String) = name == typeName

    abstract fun parse(input: String?): T?

    override fun toString(): String {
        return "DataType(name='$name')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DataType<*>) return false

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

class MetaType : DataType<String>("#datatype") {
    override fun parse(input: String?) = input
}

class BooleanType : DataType<Boolean>("boolean") {
    override fun parse(input: String?) = input?.run { toBoolean() }
}

class UnsignedLongType : DataType<ULong>("unsignedLong") {
    override fun parse(input: String?) = input?.run { toULong() }
}

class LongType : DataType<Long>("long") {
    override fun parse(input: String?) = input?.run { toLong() }
}

class DoubleType : DataType<Double>("double") {
    override fun parse(input: String?) = input?.run { toDouble() }
}

class StringType : DataType<String>("string") {
    override fun parse(input: String?) = input
}

class Base64BinaryType : DataType<ByteArray>("base64Binary") {
    private val decoder = Base64.getDecoder()

    override fun parse(input: String?) = input?.let { decoder.decode(it) }
}

class DatetimeType : DataType<Instant>("dateTime") {
    override fun canParse(typeName: String) = typeName.split(":").first() == name

    override fun parse(input: String?) = input?.let { Instant.parse(input) }
}

class DurationType : DataType<Duration>("duration") {
    override fun parse(input: String?) = input?.let { Duration.ofNanos(input.toLong()) }
}
