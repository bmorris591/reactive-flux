package uk.co.borismorris.reactiveflux

import com.influxdb.query.dsl.Flux
import kotlinx.coroutines.flow.Flow
import uk.co.borismorris.reactiveflux.webclient.DataType
import java.time.Instant
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.MILLISECONDS

interface InfluxMeasurementConverter {
    fun convert(source: Any): Flow<InfluxMeasurement>
    fun canConvert(source: Any): Boolean
}

interface InfluxClient {
    suspend fun read(query: Flux.() -> Flux): InfluxQueryResult

    fun write(points: Flow<InfluxMeasurement>): Flow<InfluxWriteSuccess>
    fun writeBatch(batch: Flow<InfluxMeasurementBatch>): Flow<InfluxWriteSuccess>
    fun convertAndWrite(points: Flow<*>): Flow<InfluxWriteSuccess>
}

fun Flow<InfluxMeasurement>.sendToInflux(influxClient: InfluxClient): Flow<InfluxWriteSuccess> = influxClient.write(this)
fun Flow<InfluxMeasurementBatch>.sendBatchToInflux(influxClient: InfluxClient): Flow<InfluxWriteSuccess> = influxClient.writeBatch(this)
fun <T> Flow<T>.convertAndSendToInflux(influxClient: InfluxClient): Flow<InfluxWriteSuccess> = influxClient.convertAndWrite(this)

data class InfluxWriteSuccess(val requestId: String)

data class InfluxQueryResult(val requestId: String, val result: Flow<InfluxResponseRow>)

interface InfluxResponseTableColumn<T : Any> {
    val index: Int
    val name: String?
    val dataType: DataType<T>
    val group: Boolean
    val default: T?
}

interface InfluxResponseTableHeader {
    val columns: List<InfluxResponseTableColumn<out Any>>
}

interface InfluxResponseRow {
    val header: InfluxResponseTableHeader
    val start: Instant
    val stop: Instant
    val time: Instant
    val value: Any
    val field: String
    val measurement: String
    val tags: Map<String, Any?>
    val tableId: Any

    fun rawValue(index: Int): String?
}

data class InfluxError(val error: String, val body: String?, val requestId: String)

class InfluxException(val error: InfluxError) : Exception(error.toString())

interface InfluxMeasurement {
    companion object {
        @JvmStatic
        fun builder(measurement: String) = InfluxMeasurementBuilder(measurement)
    }

    val measurement: String
    val tags: Map<Tag, Any>
    val fields: Map<Field, Any>
    val time: Long
    val precision: TimeUnit
}

fun InfluxMeasurement.hasFields() = fields.isNotEmpty()

fun InfluxMeasurement.hasNoFields() = fields.isEmpty()

fun InfluxMeasurement.isNotEmpty() = hasFields()

fun InfluxMeasurement.isEmpty() = hasNoFields()

private data class SimpleInfluxMeasurement(
    override val measurement: String,
    override val tags: Map<Tag, Any>,
    override val fields: Map<Field, Any>,
    override val time: Long,
    override val precision: TimeUnit,
) : InfluxMeasurement

private data class BatchedInfluxMeasurement(
    val delegate: InfluxMeasurement,
    val batchTags: Map<Tag, Any>,
) : InfluxMeasurement by delegate {

    /**
     * Ensure to add the batch tags to the measurement tags so that the batch supersedes the measurement
     */
    override val tags: Map<Tag, Any> by lazy { delegate.tags + batchTags }
}

@MeasurmentDslMarker
class InfluxMeasurementBuilder(val measurement: String) {
    private val tags: MutableMap<Tag, Any?> = HashMap()
    private val fields: MutableMap<Field, Any?> = HashMap()
    private var precision: TimeUnit = MILLISECONDS
    private var time: Long = System.currentTimeMillis()

    fun time(time: Long): InfluxMeasurementBuilder {
        this.time = time
        return this
    }

    fun time(time: Instant): InfluxMeasurementBuilder {
        this.time = time.toEpochMilli()
        this.precision = MILLISECONDS
        return this
    }

    fun precision(precision: TimeUnit): InfluxMeasurementBuilder {
        this.precision = precision
        return this
    }

    fun tag(name: Tag, value: Any?): InfluxMeasurementBuilder {
        tags[name] = value
        return this
    }

    fun tag(tag: Pair<Tag, Any?>): InfluxMeasurementBuilder {
        tags += tag
        return this
    }

    fun tags(tags: Map<Tag, Any?>): InfluxMeasurementBuilder {
        this.tags += tags
        return this
    }

    fun tags(tags: Iterable<Pair<Tag, Any?>>): InfluxMeasurementBuilder {
        this.tags += tags
        return this
    }

    fun tag(tag: String) = Tag.tag(tag)

    infix fun Tag.to(value: Any?): InfluxMeasurementBuilder {
        tag(this, value)
        return this@InfluxMeasurementBuilder
    }

    fun field(name: Field, value: Any?): InfluxMeasurementBuilder {
        fields[name] = value
        return this
    }

    fun field(field: Pair<Field, Any?>): InfluxMeasurementBuilder {
        fields += field
        return this
    }

    fun fields(fields: Map<Field, Any?>): InfluxMeasurementBuilder {
        this.fields += fields
        return this
    }

    fun fields(fields: Iterable<Pair<Field, Any?>>): InfluxMeasurementBuilder {
        this.fields += fields
        return this
    }

    fun field(field: String) = Field.field(field)

    infix fun Field.to(value: Any?): InfluxMeasurementBuilder {
        field(this, value)
        return this@InfluxMeasurementBuilder
    }

    fun build(): InfluxMeasurement = SimpleInfluxMeasurement(measurement, tags.stripNullValues(), fields.stripNullValues(), time, precision)

    @JvmName("addTag")
    operator fun Pair<Tag, String>.unaryPlus() = tag(this)

    @JvmName("addField")
    operator fun Pair<Field, Any>.unaryPlus() = field(this)
}

interface InfluxMeasurementBatch : List<InfluxMeasurement> {
    companion object {
        @JvmStatic
        fun builder() = InfluxMeasurementBatchBuilder()
    }

    val tags: Map<Tag, Any>
    val measurements: List<InfluxMeasurement>
}

private data class SimpleInfluxMeasurementBatch(
    override val tags: Map<Tag, Any>,
    override val measurements: List<BatchedInfluxMeasurement>,
) : InfluxMeasurementBatch, List<InfluxMeasurement> by measurements

@MeasurmentDslMarker
class InfluxMeasurementBatchBuilder {
    private val tags: MutableMap<Tag, Any?> = HashMap()
    private val measurements: MutableList<InfluxMeasurement> = ArrayList()

    fun tag(name: Tag, value: Any?): InfluxMeasurementBatchBuilder {
        tags[name] = value
        return this
    }

    fun tag(tag: Pair<Tag, Any?>): InfluxMeasurementBatchBuilder {
        tags += tag
        return this
    }

    fun tags(tags: Map<Tag, Any?>): InfluxMeasurementBatchBuilder {
        this.tags += tags
        return this
    }

    fun tags(tags: Iterable<Pair<Tag, Any?>>): InfluxMeasurementBatchBuilder {
        this.tags += tags
        return this
    }

    fun tag(tag: String) = Tag.tag(tag)

    infix fun Tag.to(value: Any?): InfluxMeasurementBatchBuilder {
        tag(this, value)
        return this@InfluxMeasurementBatchBuilder
    }

    fun measurement(measurement: InfluxMeasurement): InfluxMeasurementBatchBuilder {
        measurements.add(measurement)
        return this
    }

    fun measurement(measurement: String, builder: InfluxMeasurementBuilder.() -> Unit): InfluxMeasurementBatchBuilder {
        val m = InfluxMeasurement.builder(measurement)
        m.builder()
        measurement(m.build())
        return this
    }

    fun build(): InfluxMeasurementBatch = tags.stripNullValues().let { tags ->
        SimpleInfluxMeasurementBatch(
            tags,
            // concat the measurement's tags with this batch's
            measurements.map { BatchedInfluxMeasurement(it, tags) },
        )
    }
}

@Suppress("UnsafeCallOnNullableType")
private fun <K : Any, V : Any> Map<K, V?>.stripNullValues(): Map<K, V> = entries.filter { it.value != null }.associateBy({ it.key }, { it.value!! })

fun measurementBatch(builder: InfluxMeasurementBatchBuilder.() -> Unit) = InfluxMeasurementBatch.builder().also { it.builder() }.build()

fun measurement(measurement: String, builder: InfluxMeasurementBuilder.() -> Unit) = InfluxMeasurement.builder(measurement).also { it.builder() }.build()

interface Key {
    val key: String
}

data class Tag(override val key: String) : Key {
    companion object {
        @JvmStatic
        fun tag(tag: String) = Tag(tag)

        @JvmStatic
        fun of(tag: String) = tag(tag)
    }
}

data class Field(override val key: String) : Key {
    companion object {
        @JvmStatic
        fun field(field: String) = Field(field)

        @JvmStatic
        fun of(field: String) = field(field)
    }
}

@DslMarker
annotation class MeasurmentDslMarker
