package uk.co.borismorris.reactiveflux.conf

import uk.co.borismorris.reactiveflux.Consistency
import uk.co.borismorris.reactiveflux.Precision
import java.net.URL
import java.time.Duration

class InfluxDbProps {
    lateinit var url: URL
    lateinit var database: String
    var organization: String? = null
    var authToken: String? = null
    var retentionPolicy: String? = null
    var precision: Precision? = null
    var consistency: Consistency? = null
    var batchSize: Int? = null
    var batchDuration: Duration? = null
}
