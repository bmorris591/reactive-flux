package uk.co.borismorris.reactiveflux

import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.client.reactive.HttpComponentsClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import org.testcontainers.containers.InfluxDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import uk.co.borismorris.reactiveflux.webclient.WebclientInfluxClient
import java.net.URL
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit

@Testcontainers
class WebclientInfluxClientIntTest {

    companion object {
        val influxImage = DockerImageName.parse("influxdb")
    }

    @Container
    val influxdb = InfluxDBContainer(influxImage)
        .withUsername("admin")
        .withPassword("password")
        .withOrganization("default")
        .withBucket("tado")
        .withAdminToken("abc123")

    val webclient = WebClient.builder().clientConnector(HttpComponentsClientHttpConnector())

    lateinit var props: InfluxDbProps

    @BeforeEach
    fun setup() {
        props = InfluxDbProps().apply {
            url = URL(influxdb.url)
            organization = "default"
            database = "tado"
            authToken = "abc123"
            batchSize = 10
        }
    }

    @Test
    fun `Measurements are successfully written to Influx`() = runBlocking<Unit> {
        val now = Instant.now()
        val influxclient = noConverterClient()
        val measurements = flowOf(
            createMeasurement("first-measurement", time = now.epochSecond, precision = TimeUnit.SECONDS),
            createMeasurement("second-measurement", time = now.epochSecond, precision = TimeUnit.SECONDS),
            createMeasurement("third-measurement", time = now.epochSecond, precision = TimeUnit.SECONDS),
        )

        val result = influxclient.write(measurements).toList()
        assertThat(result).isNotEmpty()

        val queryResult = influxclient.read {
            range(-1L, 0, ChronoUnit.MINUTES)
        }
        assertThat(queryResult.result.toList()).hasSize(3)
    }

    private fun noConverterClient() = WebclientInfluxClient(props, webclient, emptyList())
}

fun createMeasurement(name: String, time: Long = 1000, precision: TimeUnit = TimeUnit.MINUTES) = measurement(name) {
    time(time)
    precision(precision)
    tag("test-tag") to "test-tag-value"
    field("test-field") to Math.PI
}
